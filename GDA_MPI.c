#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

int GDA_train(double *X,int *y,int N,int M, double *mu_0, double * mu_1, double *cov){
  /*
   X is the data matrix of N/p by m
   y is the target in the training data in a process
   N is the local sample size, N = N_all/P
   M is the dimension of the feature vector
	  
   return:
   P(y==1)
   mu_0
   mu_1
   covariance matrix
   */
  
  int i, j, k, count_0 = 0, count_1 = 0;
  
  // Partial mu_0 and mu_1
  for(i =0; i< N; i++){
    if(y[i]==0){
        count_0 += 1;
        for(j =0; j<M; j++){
          mu_0[j] += *(X+i*M +j);
        }
    }else{
        count_1 += 1;
        for(j =0; j<M; j++){
          mu_1[j] += *(X+i*M +j);
        }
    }
  }
  
  //Cov equation is expanded, here we only accumulate local parts X*X'
  
  for(i =0; i< N; i++){
    for(j =0; j<M; j++){
        for(k=0; k<M; k++){
          cov[j*M+k] += (*(X+i*M+j)) * (*(X+i*M+k));
        }
    }
  }

  return count_0;
}


int main(){
  #define NROW 4800000
  #define NCOL 5
  MPI_Init(NULL, NULL);
  int world_size, world_rank;
  int i=0, j, k, N, M = NCOL;
  double t1, t2;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  
  N = NROW/world_size; // Split rows to each process
  double *X_loc, *X;
  int *y_loc, *y;
  X_loc = malloc(N*M*sizeof(double));
  y_loc = malloc(N*sizeof(int));
  
  if(world_rank ==0){
    //Read in the data then call the kernel
    X = malloc(NROW*M*sizeof(double));
    y = malloc(NROW*sizeof(int));
 
    char buffer[1024];
    char * record, *line;
    FILE * fstream = fopen("data/train.csv", "r");
    printf("Starting\n");
    t1 = MPI_Wtime();
    while((line = fgets(buffer, sizeof(buffer), fstream)) != NULL){
      record = strtok(buffer, ",");
      j =0;
      while(record != NULL){
        if(j<NCOL){
          X[i*NCOL+j] = atof(record);
          //printf("%f\n", X[i*2+j]);
          record = strtok(NULL, ",");
          j++;
        }else{
          y[i] = atoi(record);
          record = strtok(NULL, ",");
        }
      }
      i++;
    }
    t2 = MPI_Wtime() - t1;
    printf("Finished reading data, time is %f\n", t2);
  }
  
  if(world_rank ==0){
    t1 = MPI_Wtime();
  }
  // Scatter data to processes
  MPI_Scatter(X, N*M, MPI_DOUBLE, X_loc, N*M, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Scatter(y, N, MPI_INT, y_loc, N, MPI_INT, 0, MPI_COMM_WORLD);
  
  
  int count_0, count_1, part_count_0, part_count_1;
  double part_mu_0[M], part_mu_1[M], part_cov[M*M];
  double mu_0[M], mu_1[M], cov[M*M];
  
  //Compute kernel
  part_count_0 = GDA_train(X_loc, y_loc, N, M, part_mu_0, part_mu_1, part_cov);
  
  //Reduce step
  part_count_1 = N - part_count_0;
  MPI_Reduce(&part_count_0, &count_0, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&part_count_1, &count_1, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  
  MPI_Reduce(part_mu_0, mu_0, M, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(part_mu_1, mu_1, M, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(part_cov, cov, M*M, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);  
 
  if(world_rank ==0){
    for(j=0; j<M; j++){
      for(k=0; k<M; k++){
        cov[j*M+k] = (cov[j*M+k] - mu_0[j]*mu_0[k]/count_0 - mu_1[j]*mu_1[k]/count_1 -
        mu_0[k]*mu_0[j]/count_0 - mu_1[k]*mu_1[j]/count_1 + mu_0[j]*mu_0[k]/count_0 +
        mu_1[j]*mu_1[k]/count_1)/NROW;
      }
    }
    t2 = MPI_Wtime() - t1;
    printf("Finished kernel, time is %f\n", t2);
    for(i =0; i<M; i++){
    for(j=0; j<M; j++){
      printf("%f ", cov[i*M+j]);
    }
    printf("\n");
    } 
    /*mu_0[0] = mu_0[0]/count_0; mu_0[1] = mu_0[1]/count_0;
    mu_1[0] = mu_1[0]/count_1; mu_1[1] = mu_1[1]/count_1;
    
    printf("mu0 are %f and %f\n", mu_0[0], mu_0[1]);
    printf("mu1 are %f and %f\n", mu_1[0], mu_1[1]);
    */
  }
  /*
  //After accumulation, calculate final Cov matrix
  for(j =0; j<2; j++){
    for(k=0; k<2; k++){
      cov[j*2+k] = (part_cov[j*2+k] - part_mu_0[j]* mu_0[k] - part_mu_1[j]* mu_1[k] -
      part_mu_0[k]* mu_0[j] - part_mu_1[k]* mu_1[j] + count_0*mu_0[j]*mu_0[k] + count_1*mu_1[j]*mu_1[k])/500;
    }
  }
  
  
  for(i =0; i<2; i++){
    for(j=0; j<2; j++){
      printf("%f ", cov[i*2+j]);
    }
    printf("\n");
  }
  */
  MPI_Finalize();
}
