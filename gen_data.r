setwd('~/Documents/Courses/UTK/COSC462_Parallel_programming/project/')

x = rnorm(500, -1.5)
X = matrix(x, ncol = 2)
x = rnorm(500, 1.5)
x = matrix(x, ncol = 2)
X = rbind(X,x)
X = as.data.frame(X)

X = cbind(1, X)
X$y = rep(c(0,1), each = 250)
write.table(X, "LR.csv", sep=',', row.names = F, col.names = F)

dat = read.csv('LR.csv', header = F)
names(dat) = c('intercpet', 'x1','x2', 'y')
gfit = glm(y~., family = binomial(link='logit'), data=dat)


### Preprocess KDD 1999 dataset
library(data.table)
dat = fread('./data/kddcup.data')
dat = as.data.frame(dat)
y = dat[1:4800000, 42]
y = ifelse(y =='normal.', 0, 1)
# Even number of test cases easy for MPI to split
dat = dat[1:4800000, c(2,3,4,5,24)]
dat[,1] = as.numeric(as.factor(dat[,1]))
dat[,2] = as.numeric(as.factor(dat[,2]))
dat[,3] = as.numeric(as.factor(dat[,3]))
dat[,4] = as.numeric(as.factor(dat[,4]))
m = apply(dat, 2, mean)
std = apply(dat, 2, sd)
temp= (t(dat) - m)/std
dat = data.frame(t(temp))

#Write out training X matrix and y vector
dat$y = y
write.table(dat, './data/train.csv',sep=',', col.names = F, row.names = F)

#Preprocess the testing dataset in a similar way
test = fread('./data/corrected')
test = data.frame(test)
y = test[, 42]
y = ifelse(y =='normal.', 0, 1)
write.table(y, './data/y.csv', sep=',',col.names = F, row.names = F )

test = test[, c(2,3,4,5,24)]
test[,1] = as.numeric(as.factor(test[,1]))
test[,2] = as.numeric(as.factor(test[,2]))
test[,3] = as.numeric(as.factor(test[,3]))
test[,4] = as.numeric(as.factor(test[,4]))
m = apply(test, 2, mean)
std = apply(test, 2, sd)
temp= (t(test) - m)/std
test = data.frame(t(temp))
write.table(test, './data/test.csv', sep=',',col.names = F, row.names = F )


#Test Logistic of our implementation with the function in R
dat = read.csv('data/train.csv', header = F)
names(dat) = c('x1', 'x2','x3','x4','x5','y')
gfit = glm(y~., family = binomial(link='logit'), data=dat)
