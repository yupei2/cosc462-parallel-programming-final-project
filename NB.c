#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

void NB(double *X, double *y, int N, int M, double* k, long* sum_xk_1, long* sum_xk_0, long* sum_1, long* sum_0){
    //Naive Bayes
    //Input X: N x M training data
    //      Y: N x 1 label
    //      k: 1 x M vector
    //Output sum_xk_1: Number of data that x_j = k and y = 1
    //       sum_xk_0: Number of data that x_j = k and y = 0
    //       sum_1: Number of data that y = 1
    //       sum_0: Number of data that y = 0
 
    int i, j, is_k;

    *sum_xk_1 = 0;
    *sum_xk_0 = 0;
    *sum_1 = 0;
    *sum_0 = 0;

    for( i=0; i<N; i++ )
    {
        is_k = 1;
        for( j=0; j<M; j++ )
            if( X[i*M+j] != k[j] )
            {
                is_k = 0;
                break;
            }
        if( y[i] == 0.0 )
        {
            (*sum_0)++;
            if( is_k )
                (*sum_xk_0)++;
        }
        else
        {
            (*sum_1)++;
            if( is_k )
                (*sum_xk_1)++;
        }
        //printf( "%f %f %d %d %d %d\n", X[i*M], y[i], *sum_xk_1, *sum_xk_0, *sum_1, *sum_0 );        
    }
}

int main(){
  #define NROW 4800000
  #define NCOL 6
  
  MPI_Init(NULL, NULL);
  int world_size, world_rank;
  int i=0, j, iter =0, N, M =NCOL;
  double t1, t2;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  
  N = NROW/world_size; // Split rows to each process
  double *X_loc, *X, *y_loc, *y, *k;
  X_loc = malloc(N*M*sizeof(double));
  y_loc = malloc(N*sizeof(double));
  k = malloc(M*sizeof(double));

  long result[4];
  long resultall[4];
  
  for( i=0; i<4; i++)
      resultall[i]=0;
  
  double theta[M], grad[M], H[M*M], temp[M];
  double part_grad[M], part_H[M*M];

  memset(theta, 0, sizeof(double)*M);
  memset(grad, 0, sizeof(double)*M);
  memset(H, 0, sizeof(double)*M*M);
  
  if(world_rank ==0){
    X = malloc(NROW*M*sizeof(double));
    y = malloc(NROW*sizeof(double));
    
    char buffer[1024];
    char * record, *line;
    FILE * fstream = fopen("data/train.csv", "r");
    printf("Starting\n");
    t1 = MPI_Wtime();
    while((line = fgets(buffer, sizeof(buffer), fstream)) != NULL){
      record = strtok(line, ",");
      X[i*NCOL] = 1.;
      j =1;
      while(record != NULL){
        if(j<NCOL){
          X[i*NCOL+j] = atof(record);
          record = strtok(NULL, ",");
          j++;
        }else{
          y[i] = atof(record);
          record = strtok(NULL, ",");
        }
      }
      i++;
    }
    t2 = MPI_Wtime() - t1;
    printf("Finished reading data, time is %f\n", t2);
  }
  //if(world_rank==0){
  //  t1 = MPI_Wtime();
  //}
  // Scatter data to processes
  MPI_Scatter(X, N*M, MPI_DOUBLE, X_loc, N*M, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Scatter(y, N, MPI_DOUBLE, y_loc, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  //Specift k here
  if(world_rank==0)
  {
      for( j=0; j<M ; j++ )
          k[j]=X[ 123*M+j ];
  }
  MPI_Bcast( k, M, MPI_DOUBLE, 0, MPI_COMM_WORLD );

  if(world_rank==0){
    t1 = MPI_Wtime();
  }

  NB(X_loc, y_loc, N, M, k, &result[0], &result[1], &result[2], &result[3]);

  MPI_Reduce(result, resultall, 4, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
    
  if(world_rank ==0){
    t2 = MPI_Wtime() - t1;
    printf("Finished kernel, time is %f\n", t2);
    for(j=0; j<4; j++){
        printf("result %d is %ld\n", j , resultall[j]);
    } 
  }
  MPI_Finalize();
}
