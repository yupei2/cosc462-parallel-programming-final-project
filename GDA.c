#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

double GDA_train(double *X,int *y,int N,int M, double *mu_0, double * mu_1, double *cov){
	/*
      X is the data matrix of n by m
      y is the target in the training data
      N is the local sample size
      M is the dimension of the feature vector
	  
	  return: 
	  P(y==1)
	  mu_0
	  mu_1
	  covariance matrix
	*/
    int i, j, k, count_0 = 0, count_1 = 0;
    double p; 

    for(i =0; i< N; i++){
    	if(y[i]==0){
    		count_0 += 1;
    		for(j =0; j<M; j++){
    			mu_0[j] += *(X+i*M +j);
    		}
    	}else{
    		count_1 += 1;
    		for(j =0; j<M; j++){
    			mu_1[j] += *(X+i*M +j);
    		}
    	}
    }
    p = (double)count_1/N;
    for(j =0; j<M; j++){
    	mu_0[j] /= count_0;
    	mu_1[j] /= count_1;
    }
    for(i =0; i< N; i++){
    	for(j =0; j<M; j++){
    		for(k=0; k<M; k++){
    			if(y[i] ==0){
    				cov[j*M+k] += (*(X+i*M+j) - mu_0[j])*(*(X+i*M+k) - mu_0[k]);
    			}else{
    				cov[j*M+k] += (*(X+i*M+j) - mu_1[j])*(*(X+i*M+k) - mu_1[k]);
    			}
    		}
    	}
    }
    for(j =0; j<M; j++){
    	for(k =0; k<M; k++){
    		cov[j*M+k] /= N;
    	}
    }
    return p;
}



int main(){
	//Read in the data then call the kernel
	#define NROW  4800000
        #define NCOL  5
        MPI_Init(NULL,NULL);
        double t1, t2;
        double X[NROW*NCOL];
	int y[NROW], i=0, j;
	char buffer[1024];
	char * record, *line;
	FILE * fstream = fopen("data/train.csv", "r");
	printf("Starting\n");
        t1 = MPI_Wtime();
	while((line = fgets(buffer, sizeof(buffer), fstream)) != NULL){
		record = strtok(buffer, ",");
		j =0;
		while(record != NULL){
			if(j<NCOL){
				X[i*NCOL+j] = atof(record);
				//printf("%f\n", X[i*2+j]);
				record = strtok(NULL, ",");
				j++;
			}else{
				y[i] = atoi(record);
				record = strtok(NULL, ",");
			}
		}
		i++;
	}
    t2 = MPI_Wtime();
    printf("Finished reading data, time is %f\n", t2-t1);
    
    t1 = MPI_Wtime();
    double mu_0[NCOL]={0.}, mu_1[NCOL]={0.}, cov[NCOL*NCOL]={0.};
    GDA_train(X, y, NROW, NCOL, mu_0, mu_1, cov);
    t2 = MPI_Wtime();
    printf("Finished kernel, time is %f\n", t2-t1);
 for(i=0; i<NCOL;i++){
  printf("mu at %d are %f and %f\n", i, mu_0[i], mu_1[i]);
 }
  
 for(i =0; i<NCOL; i++){
    for(j=0; j<NCOL; j++){
      printf("%f ", cov[i*NCOL+j]);
    }
    printf("\n");
  }
  MPI_Finalize();
}
