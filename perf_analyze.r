setwd('~/Documents/Courses/UTK/COSC462_Parallel_programming/project/')
library(ggplot2)

br = c(1,2,4,8,16)
dat = read.csv('perf_data.csv')
temp = split(dat$Time, dat$Kernel)
dat$Speedup = unlist(lapply(temp, function(x)x[1]/x))
gobj = ggplot(dat, aes(x = Cores, y= Speedup, colour=Kernel)) + 
  geom_line(size = 1.5) + scale_x_continuous(trans = 'log2', breaks = br) + 
  scale_y_continuous(trans = 'log2', breaks = br) +
  theme(text = element_text(size= 24), legend.text = element_text(size = 20))
