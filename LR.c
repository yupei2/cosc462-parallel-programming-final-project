#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);
void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);

double sigmoid(double *x,double* theta, int M){
  int i;
  double temp;
  for(i=0; i< M; i++){
    temp -= theta[i]*x[i];
  }
  return 1/(1+ exp(temp));
}

void LR_train(double *X, double *y, int N, int M, double *theta, double*grad, double *H){
  /*
    Newton Raphson algorithm iteration to solve for parameters
    X data matrix with a column of 1 for intercept term
    y target, 0 or 1
    N sample size
    M dimension of features (including the intercept term)
    
    Output:
    theta: current parameters
    grad: gradient for this iteration
    H: Hessian for this iteration
  */
  int i, j, k;
  double sig, temp1, temp2;
  memset(grad, 0, sizeof(double)*M);
  memset(H, 0, sizeof(double)*M*M);
  
  //Solve for gradient and Hessian
  for(i=0; i<N; i++){
    
    sig = sigmoid(X+i*M, theta, M);
    temp1 = sig - y[i];
    temp2 = sig*(1-sig);
    
    for(j=0; j<M; j++){
      grad[j] += temp1*X[i*M+j];
    }
    
    for(j=0; j<M; j++){
      for(k=0; k<M; k++){
        H[j*M+k] += temp2*X[i*M+j]*X[i*M+k];
      }
    }
  }
  
  for(j=0; j<M; j++){
    grad[j] /= N;
  }
  for(j =0; j<M; j++){
    for(k=0; k<M; k++){
      H[j*M+k] /= N;
    }
  }
  
}

void inverse(double* A, int N)
{
  int *IPIV = malloc(N*sizeof(int));
  int LWORK = N*N;
  double *WORK = malloc(LWORK*sizeof(double));
  int INFO;
  
  dgetrf_(&N,&N,A,&N,IPIV,&INFO);
  dgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);
  
  free(IPIV);
  free(WORK);
}

int main(){
  
  //Read in the data then call the kernel
  #define NROW 4800000
  #define NCOL 6
  MPI_Init(NULL, NULL);
  double X[NROW*NCOL], y[NROW]; // With intercept term added
  double t1, t2;
  double theta[NCOL], grad[NCOL], H[NCOL*NCOL], temp[NCOL];
  memset(theta, 0, sizeof(double)*NCOL);
  memset(grad, 0, sizeof(double)*NCOL);
  memset(H, 0, sizeof(double)*NCOL*NCOL);
  int i=0, j, k, iter =0;
  char buffer[1024];
  char * record, *line;
  FILE * fstream = fopen("data/train.csv", "r");
  printf("Starting\n");
  t1 = MPI_Wtime();
  while((line = fgets(buffer, sizeof(buffer), fstream)) != NULL){
    record = strtok(line, ",");
    X[i*NCOL] = 1.;// add the intercept
    j =1;
    while(record != NULL){
      if(j<NCOL){
        X[i*NCOL+j] = atof(record);
        record = strtok(NULL, ",");
        j++;
      }else{
        y[i] = atof(record);
        record = strtok(NULL, ",");
      }
    }
    i++;
  }
  t2 = MPI_Wtime() - t1;
  printf("Finished reading data, time is %f\n", t2);
  t1 =MPI_Wtime();
  while(iter <15){
    
  LR_train(X, y, NROW, NCOL, theta, grad, H);
  
  inverse(H, NCOL);
  /*
  for(j=0; j<3; j++){
    printf("gradient %d is %f\n",j, grad[j]);
  }
  for(j =0; j<3; j++){
    for(k=0; k<3; k++){
      printf("%f ", H[j*3+k]);
    }
    printf("\n");
  }
  */
    memset(temp, 0, sizeof(double)*NCOL);
    for(j =0; j<NCOL; j++){
      for(k=0; k<NCOL; k++){
        temp[j] += H[j*NCOL+k]*grad[k];
      }
      theta[j] = theta[j] - temp[j];
    }
    
    //for(j=0; j< NCOL; j++){
    //  printf("theta %d is %f\n",j, theta[j]);
    //}
    iter++;
  }
  t2 = MPI_Wtime() - t1;
  printf("Finished kernel, time is %f\n", t2);
  MPI_Finalize(); 
}
