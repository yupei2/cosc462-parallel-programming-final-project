#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

void SVM(double *X, double *y, double* w, int N, int M, double* k){
    //Linear SVM by gradient descent
    //Input X: N x M training data
    //      Y: N x 1 label
    //      w: 1 x M weight
    //Output k: 1 x M partial gradient
 
    int i, j;
    double tmp;

    for( i=0; i<M; i++)
        k[i]=0.0;

    for( i=0; i<N; i++ )
    {
        tmp = -y[i];
        for( j=0; j<M; j++)
            tmp += w[j]*X[i*M+j];

        for( j=0; j<M; j++)
            k[j] += tmp*X[i*M+j];
        //printf("%f %f %f\n", tmp, k[0], k[1]);
    }
}

int main(){
  #define NROW 4800000
  #define NCOL 6
  
  MPI_Init(NULL, NULL);
  int world_size, world_rank;
  int i=0, j, iter =0, N, M =NCOL;
  double t1, t2, rate=1e-10;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  
  N = NROW/world_size; // Split rows to each process
  double *X_loc, *X, *y_loc, *y, *k, *w, *kall;
  X_loc = malloc(N*M*sizeof(double));
  y_loc = malloc(N*sizeof(double));
  k = malloc(M*sizeof(double));
  kall = malloc(M*sizeof(double));
  w = malloc(M*sizeof(double));

  long result[4];
  long resultall[4];
  
  for( i=0; i<4; i++)
      resultall[i]=0;
  
  double theta[M], grad[M], H[M*M], temp[M];
  double part_grad[M], part_H[M*M];

  memset(theta, 0, sizeof(double)*M);
  memset(grad, 0, sizeof(double)*M);
  memset(H, 0, sizeof(double)*M*M);
  
  if(world_rank ==0){
    X = malloc(NROW*M*sizeof(double));
    y = malloc(NROW*sizeof(double));
    
    char buffer[1024];
    char * record, *line;
    FILE * fstream = fopen("data/train.csv", "r");
    printf("Starting\n");
    t1 = MPI_Wtime();
    while((line = fgets(buffer, sizeof(buffer), fstream)) != NULL){
      record = strtok(line, ",");
      X[i*NCOL] = 1.;
      j =1;
      while(record != NULL){
        if(j<NCOL){
          X[i*NCOL+j] = atof(record);
          record = strtok(NULL, ",");
          j++;
        }else{
          y[i] = atof(record);
          record = strtok(NULL, ",");
        }
      }
      i++;
    }
    t2 = MPI_Wtime() - t1;
    printf("Finished reading data, time is %f\n", t2);
  }
  //if(world_rank==0){
  //  t1 = MPI_Wtime();
  //}
  // Scatter data to processes
  MPI_Scatter(X, N*M, MPI_DOUBLE, X_loc, N*M, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Scatter(y, N, MPI_DOUBLE, y_loc, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  //Specift initial w here
  if(world_rank==0)
  {
      for( j=0; j<M ; j++ )
          w[j]=0.0;
  }

  if(world_rank==0){
    t1 = MPI_Wtime();
  }
  while( 50 > iter++ )
  {

    MPI_Bcast( w, M, MPI_DOUBLE, 0, MPI_COMM_WORLD );
    SVM(X_loc, y_loc, w, N, M, k);
    MPI_Reduce(k, kall, M, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    for( j=0; j<M ; j++ )
        w[j]+=rate*kall[j];
  }
    
  if(world_rank ==0){
    t2 = MPI_Wtime() - t1;
    printf("Finished kernel, time is %f\n", t2);
    for(j=0; j<M; j++){
        printf("w[%d] is %f\n", j , w[j]);
    } 
  }
  MPI_Finalize();
}
