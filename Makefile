CC = mpicc

all: GDA GDA_MPI LR LR_MPI NB SVM

GDA: GDA.c
	$(CC) GDA.c -o GDA
GDA_MPI: GDA_MPI.c
	$(CC) GDA_MPI.c -o GDA_MPI
LR: LR.c
	$(CC) LR.c -mkl -o LR
LR_MPI: LR_MPI.c
	$(CC) LR_MPI.c -mkl -o LR_MPI

NB: NB.c
	$(CC) $< -o $@

SVM: SVM.c
	$(CC) $< -o $@

clean:
	rm GDA GDA_MPI LR LR_MPI NB SVM
