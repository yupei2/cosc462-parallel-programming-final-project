## Repository for parallel programming(cosc462) final project

Author: Mike Tsai and Yu Pei

Here we implemented four classification algorithms with MPI, replicating the results from a 2006 NIPS paper. The purpose is to demostrate the scalibility of the training method.


TODO:

- Add timing calls
- Add prediction function to predict on new data

